# Prime Factors TDD Kata

## From Bob Martin Slide

> This is a very short Kata. The final algorithm is three lines of code. Interestingly enough there are 40 lines of test code.

> Although quite short, this kata is fascinating in the way it shows how ‘if’ statements become ‘while’ statements as the number of test cases increase. It’s also a wonderful example of how algorithms sometimes become simpler as they become more general.

> I stumbled upon this little kata one evening when my son was in 7th grade. He had just discovered that all numbers can be broken down into a product of primes and was interested in exploring this further. So I wrote a little ruby program, test-first, and was stunned by how the algorithm evolved.

> I have done this particular kata in Java 5.0. This should give you a feel for the power and convenience of some of the new features.> 

http://butunclebob.com/files/downloads/Prime%20Factors%20Kata.ppt


## Requirement


Write a static method to return the prime number factors of a given number.

Create a PrimeFactors class.

```java
public class PrimeFactors {

    public static List<Integer> generate(int n) {

    }

}
```

Create a unit test class
```
class PrimeFactorsTest
```

### First Test

Return an  empty list when number is 1
#### Note: 1 is not a prime numbers.

```
    @Test
    public void one() {
        assertEquals(list(), generate(1));
    }


    // Expected result
    private List<Integer> list(int... n) {
        List<Integer> l = new ArrayList<>();
        for (int i : n) {
            l.add(i);
        }
        return l;
    }

```



### Second Test

Return 2 when number is 2

```
    @Test
    public void two() {
        assertEquals(list(2), generate(2));
    }

```

### Third Test

Return 3 when number is 3

```
    @Test
    public void three() {
        assertEquals(list(3),  generate(3));
    }

```

### Fourth to Seventh Test

Return 2, 2 when number is 4

```
    @Test
    public void four() {
        assertEquals(list(2, 2),  generate(4));
    }

    @Test
    public void six() {
       assertEquals(list(2, 3), generate(6));
    }

   @Test
   public void eight() {
      assertEquals(list(2, 2, 2), generate(8));
   }

   @Test
   public void eight() {
      assertEquals(list(3, 3), generate(9));
   }

```


## Coding Steps

### First step

```java
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    return new ArrayList<Integer>();​

  }​

}
```

### Second step

```

public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​

    if (n > 1) {​
      primes.add(2);​
    }​

    return primes;​

  }​

}

```

### Third step

public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​

    if (n > 1) {​

      primes.add(n);​ // <--- 2 > n

    }​

    return primes;​

  }​

}

### Fourth to Seventh step

```java
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​

    if (n > 1) {​
      if (n%2 == 0) {​
        primes.add(2);​
        n /= 2;​
      }​

      if (n > 1)​
        primes.add(n);​

    }​

    return primes;​

  }​
}
```


```java
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​

    if (n > 1) {​
      while (n%2 == 0) {​ /// while
        primes.add(2);​
        n /= 2;​
      }​

      if (n > 1)​
        primes.add(n);​

    }​

    return primes;​

  }​
}
```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​

    if (n > 1) {​
      int candidate = 2;​
      while (n%candidate == 0) {​
        primes.add(candidate);​
        n /= candidate;​
      }​

      if (n > 1)​
        primes.add(n);​

    }​

    return primes;​
  }​
}​

```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​

    if (n > 1) {​
      int candidate = 2;​
      while (n % candidate == 0) {​
        primes.add(candidate);​
        n /= candidate;​
      }​
    }​

    if (n > 1)​  // <<
      primes.add(n);​

    return primes;​

  }​

}
```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​
    int candidate = 2;​  // <<
    if (n > 1) {​
      while (n % candidate == 0) {​
        primes.add(candidate);​
        n /= candidate;​
      }​
    }​

    if (n > 1)​
      primes.add(n);​

    return primes;​

  }​
}
```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​
    int candidate = 2;​
    while (n > 1) {​  // <<
      while (n % candidate == 0) {​
        primes.add(candidate);​
        n /= candidate;​
      }​
      candidate++;​
    }​

    if (n > 1)​
      primes.add(n);​

    return primes;​
  }​
}
```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​
    int candidate = 2;​
    while (n > 1) {​  // <<
      while (n % candidate == 0) {​
        primes.add(candidate);​
        n /= candidate;​
      }​
      candidate++;​
    }​

/// >>    if (n > 1)​
/// >>     primes.add(n);​

    return primes;​
  }​
}
```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​
    int candidate = 2;​
    while (n > 1) {​  // <<
      for (; n % candidate == 0; n /= candidate) {​ // <<<
        primes.add(candidate);​
      }​
      candidate++;​
    }​

    return primes;​
  }​
}
```

```
public class PrimeFactors {​

  public static List<Integer> generate(int n) {​

    List<Integer> primes = new ArrayList<Integer>();​
    for (int candidate = 2; n > 1; candidate++) {​  // <<
      for (; n % candidate == 0; n /= candidate) {​
        primes.add(candidate);​
      }​
    }​

    return primes;​
  }​
}
```